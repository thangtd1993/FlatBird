﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BirdScript : GameController {
    public static float minHeight = 0.5f;
    public static float minWidth = 0.5f;
    public static float minWeight = 0.6f;

    public float maxFly = 5.55f;
    public float flyForce = 3f;

    public static bool isDead;
    public static bool isStart;
    public static int score;

    public static GameObject birdObj;
    public GameObject coinObj;
    public Rigidbody2D birdBody;

    // Use this for initialization
    void Start () {
        isDead = false;
        score = 0;
        _isStartGame();
        birdObj = GameObject.FindGameObjectWithTag("Bird");
        birdObj.transform.localScale = new Vector3(minHeight, minWidth, transform.position.z);
        birdBody = GetComponent<Rigidbody2D>();
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        _isStartGame();
        if (Input.GetMouseButtonDown(0) && transform.position.y < maxFly)
        {
            birdBody.velocity = new Vector2(birdBody.velocity.x, flyForce);
        }
	}

    void _isStartGame()
    {
        if (!isStart && Input.GetMouseButtonDown(0))
        {            
            isStart = true;
        }

        if (isStart)
        {
            birdBody.gravityScale = minWeight;
        }
        else
        {
            birdBody.gravityScale = 0;
        }
    }    

    public static void getPoinAndGrown()
    {
        score += 1;
            minWidth += 0.01f;
            minHeight += 0.01f;
            minWeight += 0.01f;
        birdObj.transform.localScale = new Vector3(minHeight, minWidth, birdObj.transform.position.z);
    }

    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Land")
        {
            isDead = true;
        }
    }
}
