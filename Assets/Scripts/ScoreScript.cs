﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : GameController
{
    public Text scoreText;
    public Text highScoreText;
    // Use this for initialization
    void Start () {
        highScore = PlayerPrefs.GetInt("highScore", highScore);
        highScoreText.text = "High Score: " + highScore;
    }
	
	// Update is called once per frame
	void Update () {
        scoreText.text = "x " + BirdScript.score;
	}
}
