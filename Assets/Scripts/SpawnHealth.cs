﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnHealth : GameController
{
    [SerializeField]
    private GameObject healthObj;
    // Use this for initialization
    void Start () {
        StartCoroutine(Spawn());
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(5f);
        Vector3 healthOffSet = healthObj.transform.position;
        healthOffSet.y = Random.Range(-1f, 5.15f);

        if (BirdScript.isStart)
        {
            Instantiate(healthObj, healthOffSet, Quaternion.identity);
        }
        StartCoroutine(Spawn());
    }
}
