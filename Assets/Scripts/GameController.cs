﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameController : MonoBehaviour {
    public Text gameMessage;
    public int highScore;
    public RectTransform panel;
    // Use this for initialization
    void Start () {
        gameMessage = GetComponent<Text>();
        panel = GetComponent<RectTransform>();        
    }
	
	// Update is called once per frame
	void FixedUpdate () {
        _checkDead();
    }

    private void _checkDead()
    {
        if (BirdScript.isDead)
        {
            Time.timeScale = 0;
            gameMessage.text = "You Dead";
            highScore = PlayerPrefs.GetInt("highScore", highScore);
            if (BirdScript.score > highScore)
            {
                highScore = BirdScript.score;
                PlayerPrefs.SetInt("highScore", highScore);
            }            
            panel.gameObject.SetActive(true);
        }
    }
}
