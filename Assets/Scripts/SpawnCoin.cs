﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoin : GameController
{
    [SerializeField]
    private GameObject coinObj;
    // Use this for initialization
    void Start () {
        StartCoroutine(Spawn());       
    }

    IEnumerator Spawn()
    {
        yield return new WaitForSeconds(1.5f);
        Vector3 coinOffSet = coinObj.transform.position;
        coinOffSet.y = Random.Range(-1f, 5.15f);    
        
        if (BirdScript.isStart)
        {
            Instantiate(coinObj, coinOffSet, Quaternion.identity);
        }
        StartCoroutine(Spawn());
    }
}
