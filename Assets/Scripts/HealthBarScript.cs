﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBarScript : GameController
{
    public static float currentHealth;
    public static float maxHealth;
    public static float regenHealth;
    public Slider healthBar;
	// Use this for initialization
	void Start () {
        maxHealth = 30f;
        regenHealth = 5f;
        currentHealth = maxHealth;
        healthBar.value = CaculatetorHealth();
        StartCoroutine(ExecuteAfterTime(1));
    }
	
	// Update is called once per frame
	void Update () {
        if(currentHealth == 0)
        {
            BirdScript.isDead = true;
        }
    }

    IEnumerator ExecuteAfterTime(float time)
    {
        yield return new WaitForSeconds(time);
        if (BirdScript.isStart)
        {
            currentHealth -= 1f;
            healthBar.value = CaculatetorHealth();
        }        
        StartCoroutine(ExecuteAfterTime(1));
    }

    float CaculatetorHealth()
    {
        return currentHealth / maxHealth;
    }

    public static void _RegenHealth()
    {
        if((maxHealth-currentHealth) >= regenHealth)
            currentHealth += regenHealth;
    }
}
