﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundScript : GameController
{
    public float speed = 3.5f;
    public float size = 24.2f;
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {
        if (!BirdScript.isDead)
        {
            _GroundMove();
        }
    }

    void _GroundMove()
    {
        Vector3 offset = transform.position;
        offset.x -= speed * Time.deltaTime;
        if(offset.x < -25f)
        {
            offset.x = offset.x+size*2;
        }
        transform.position = offset;
    }
}
