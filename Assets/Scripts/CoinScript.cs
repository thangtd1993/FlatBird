﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CoinScript : GameController
{
    float x;
    public float speed = 3.5f;

    // Use this for initialization
    void Start () {
        
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        if (BirdScript.isStart && !BirdScript.isDead)
        {
            _CoinMove();
        }
        _DestroyCoin();
    }

    

    void _CoinMove()
    {
        Vector3 offset = transform.position;
        offset.x -= speed * Time.deltaTime;
        transform.position = offset;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bird")
        {
            BirdScript.getPoinAndGrown();            
            Destroy(this.gameObject);
        }
    }

    void _DestroyCoin()
    {
        if(transform.position.x < -25f)
        {
            Destroy(gameObject);
        }
    }
}
