﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthScript : GameController
{
    float x;
    public float speed = 3.5f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void FixedUpdate() {
        if (BirdScript.isStart && !BirdScript.isDead)
        {
            _HealthMove();
        }
        _DestroyHealth();
    }

    void _HealthMove()
    {
        Vector3 offset = transform.position;
        offset.x -= speed * Time.deltaTime;
        transform.position = offset;
    }

    void _DestroyHealth()
    {
        if (transform.position.x < -25f)
        {
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Bird")
        {
            HealthBarScript._RegenHealth();
            Destroy(this.gameObject);
        }
    }
}
